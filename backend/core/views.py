from rest_framework import viewsets

from core.models import OfficeSpace
from core.serializers import OfficeSpaceSerializer


class OfficeSpaceViewSet(viewsets.ModelViewSet):
    """TODO: at least queryset and serializer_class are missing """
    serializer_class = OfficeSpaceSerializer
    queryset = OfficeSpace.objects.all()
