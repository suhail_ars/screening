from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.contrib.auth.models import BaseUserManager

from core.constants import USER_CHOICES


class UserManager(BaseUserManager):
    
    def create_superuser(self, email, password, **kwargs):
        user = self.model(
            email=self.normalize_email(email),
            is_superuser=True,
            is_staff=True,
            **kwargs,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):
    USERNAME_FIELD = 'email'
    email = models.EmailField(max_length=255, unique=True)
    type = models.CharField(max_length=8, choices=USER_CHOICES)
    is_staff = models.BooleanField(default=False)
    
    objects = UserManager()


class OfficeSpace(models.Model):
    name = models.CharField(max_length=100)
    street = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    zip_code = models.CharField(max_length=100)
    public = models.BooleanField(default=True)

    lat = models.DecimalField(max_digits=10, decimal_places=7)
    lng = models.DecimalField(max_digits=10, decimal_places=7)
