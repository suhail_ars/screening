from decimal import Decimal
import random
import argparse

from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management import BaseCommand

from core.constants import LANDLORD, TENANT
from core.models import CustomUser, OfficeSpace

User = get_user_model()


def office_space_factory(number):
    building_names = [
        'Central Tower', 'Centrum LIM', 'Generation Park', 'Intraco I',
        'Millennium Plaza', 'Oxford Tower', 'Palace of Culture and Science',
    ]
    for i in range(number):
        yield {
            # TODO make building names unique
            'name': building_names[i % len(building_names)],
            'street': 'Street 1',
            'city': 'City',
            'zip_code': '01-123',
            'lat': Decimal(random.randint(52, 53)),
            'lng': Decimal(random.randint(20, 21)),
        }

def user_factory(number):
    user_names = [
        'suhail@gmail.com', 'leo@gmail.com', 'messi@gmail.com', 'torres@gmail.com',
        'neymar@gmail.com', 'iniesta@gmail.com', 'xavi@gmail.com',
    ]
    for i in range(number):
        yield {
            # TODO make building names unique
            'email': user_names[i % len(user_names)],
            'type': random.choice([LANDLORD, TENANT])
        }

# to avoid duplicate names
def number_type(x):
    x = int(x)
    if x > 6:
        raise argparse.ArgumentTypeError("Max limit is 6")
    return x

class Command(BaseCommand):
    """
    Populates local database for development with sample random
    objects.

    Never to be used in production.
    # TODO: prevent running in production.

    """
    # TODO add user factory and user number argument
    def add_arguments(self, parser):

        parser.add_argument(
            '--office-spaces',
            type=number_type,
            default=6
        )
        parser.add_argument(
            '--user-spaces',
            type=number_type,
            default=6
        )

    def handle(self, *args, **options):
        office_spaces = options.get('office_spaces', None)
        user_spaces = options.get('user_spaces', None)
        if not settings.DEBUG and (office_spaces or user_spaces):
            self.stdout.write(self.style.ERROR('Not allowed in production'))
            return    
        for office_space in office_space_factory(office_spaces):
            OfficeSpace.objects.create(**office_space)
        for user_space in user_factory(user_spaces):
            CustomUser.objects.create(**user_space)

        CustomUser.objects.create_superuser(email="admin@admin.com", password="admin", type=TENANT)    
        self.stdout.write(self.style.SUCCESS('Successfully Populates'))