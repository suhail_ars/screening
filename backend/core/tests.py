# -*- coding: utf-8 -*-
from __future__ import unicode_literals

"""
TODO: a test or two would be useful.
Up to you whether you want a unit test (meh, there's so little to unit test here)
or e2e test using rest_framework.test.APIClient (yeah).
"""
from django.test import TestCase

# Create your tests here.
from rest_framework.test import APITestCase
from rest_framework.test import APIClient



connection = APIClient()
class officeSpaceTests(APITestCase):
    def test_retrive_office_space_list(self):        
        response = connection.get('/office-spaces/', format='json')
        self.assertEqual(response.status_code, 200)